import { Injectable } from '@angular/core';
import { NativeStorage } from 'ionic-native';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Preferences provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Preferences {
  
  providers: [NativeStorage]

  constructor(public http: Http) {
    console.log('Hello Preferences Provider');

  }


public get(){
    return new Promise((resolve, reject) => {
        NativeStorage.getItem('user')
          .then( function (data) {
               resolve(data);
            }, function (error) {
              reject(error)
            });
      });
    }
}



