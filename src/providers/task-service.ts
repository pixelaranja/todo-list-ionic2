import { Injectable } from '@angular/core';
import { NativeStorage } from 'ionic-native';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the TaskService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TaskService {

  baseUri: string
  headers: Headers;
  options: RequestOptions;


  constructor(public http: Http) {
     this.baseUri = "http://totalcommit-todolist.herokuapp.com/api/v1";
     this.headers = new Headers({ 'Content-Type': 'application/json', 
                                   'X-Client-Access-Token': 's30i0gydUo-dwhvoPIs_Ffitkl6c3tvMNHYX3Zk072NFCQoVFL9jX6QAqAZHQuF55x9f6t9VH8qneYN429iEqSxOd0M2fniPKqlJzHuORtt-f1Zqk1iya6Jx9aYdomKNGkgsH1lv84murgnwCzdRaeA3xJPAzA_HXcFP1nOOZo1acttr8eRZaIoH1_LYpbipMEr47LHKadrqOX-GmrubcotkMS-e2aPN9ls1mD3pC3Y=' });
     this.options = new RequestOptions({ headers: this.headers });

  }


  // GET 
  taskAll(uuid){
    return new Promise((resolve, reject) => {
        this.http.get(this.baseUri + "/users/"+uuid+"/tasks")
           .map(res => res.json())
           .subscribe(data =>{ 
              resolve(data.tasks);
           }, error => {
               reject(error)
           });
    });
  }


  // POST
public taskPost(url,json){
    return new Promise((resolve, reject) => {
        this.http.post(this.baseUri + url, json ,this.options)
           .map(res => res.json())
           .subscribe(data =>{ 
              resolve(data);
           }, error => {
               reject(error)
           });
    });
  }


  // UPDATE
  taskUpdate(url,uuid, body){
    return new Promise((resolve, reject) => {
        this.http.put(this.baseUri + url + uuid, body ,this.options)
           .map(res => res.json())
           .subscribe(data =>{ 
              resolve(data.tasks);
           }, error => {
               reject(error)
           });
    });
  }


  // DELETE
  taskDelete(uuid){
    return new Promise((resolve, reject) => {
        this.http.delete(this.baseUri + "/tasks/" + uuid, this.options)
           .map(res => res.json())
           .subscribe(data =>{ 
              resolve(data.tasks);
           }, error => {
               reject(error)
           });
    });
  }




}
