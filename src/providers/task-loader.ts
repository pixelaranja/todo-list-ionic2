import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the TaskLoader provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TaskLoader {
  
  mLoading: any

  constructor(public http: Http, private taskload: LoadingController) {
      
      this.mLoading = taskload.create({
           content: 'Please wait...'
      });
  }


  show(){
    this.mLoading.present();
  }

  hide(){
    this.mLoading.dismiss();
  }


}
