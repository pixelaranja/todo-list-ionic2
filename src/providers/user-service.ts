import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { NativeStorage } from 'ionic-native';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserService {

  baseUri: string
  headers: Headers;
  options: RequestOptions;

  constructor(public http: Http) {
     this.baseUri = "http://totalcommit-todolist.herokuapp.com/api/v1";
     this.headers = new Headers({ 'Content-Type': 'application/json'});
     this.options = new RequestOptions({ headers: this.headers });
  }



  get(uuid){
    return new Promise((resolve, reject) => {
        this.http.get(this.baseUri + "/users/"+uuid)
           .map(res => res.json())
           .subscribe(data =>{ 
              resolve(data.user);
           }, error => {
               reject(error)
           });
    });

  }


  create(url,json){
     return new Promise((resolve, reject) => {
        this.http.post(this.baseUri + url, json ,this.options)
           .map(res => res.json())
           .subscribe(data => { 
              resolve(data.account);
           }, error => {
               reject(error)
           });
     });
   }



}
