import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, App } from 'ionic-angular';
import { StatusBar, Splashscreen, NativeStorage } from 'ionic-native';
import { TaskService } from '../providers/task-service'
import { UserService } from '../providers/user-service'
import { TaskLoader } from '../providers/task-loader';
import { HomePage } from '../pages/home/home';
import {LoginPage } from '../pages/login/login';


@Component({
  templateUrl: 'app.html',
  providers: [TaskService, UserService, TaskLoader]
})
export class MyApp{

  // rootPage = LoginPage;

  constructor(platform: Platform, private app: App) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
       this.startPage();
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

   startPage(){
       let nav = this.app.getActiveNav();
        NativeStorage.getItem('user')
          .then( function (data) {
                nav.push(HomePage);
                Splashscreen.hide();
              }, function (error) {
                nav.push(LoginPage);
                Splashscreen.hide();
          });
   }
}