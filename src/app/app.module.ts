import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile'
import { MyListPage } from '../pages/my-list/my-list'
import { TaskModalPage } from '../pages/task-modal/task-modal';
import {LoginPage } from '../pages/login/login';
import {WelcomePage} from '../pages/welcome/welcome'



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ProfilePage,
    MyListPage,
    TaskModalPage,
    LoginPage,
    WelcomePage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ProfilePage,
    MyListPage,
    TaskModalPage,
    LoginPage,
    WelcomePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
