import { Response } from '@angular/http';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Facebook, NativeStorage, LocalNotifications} from 'ionic-native';
import { Preferences } from './../../providers/preferences';
import { UserService } from './../../providers/user-service'
import { TaskLoader } from './../../providers/task-loader';
import { LoginPage } from '../../pages/login/login';

/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [UserService, Preferences,TaskLoader]
})
export class ProfilePage {

  private rootPage:any;
  private picture: string;
  private userName: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserService, private preferences: Preferences, private taskLoader: TaskLoader) {
      this.getUser();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    //this.getUser();
  }


  getUser(){
      this.taskLoader.show();
      this.preferences.get()
         .then((data: any) =>{
              console.log("____________________",data.uuid);
              this.userService.get(data.uuid)
                 .then((response: any) => {
                  console.log(response);
                  this.taskLoader.hide();
                  this.picture = response.picture;
                  this.userName = response.name;
              }, (erro) => {
                  console.log(erro);
                  this.taskLoader.hide();
             });

        }, (erro) => {
           console.log(erro);
    })
  }




  public btLogout(){
    let nav = this.navCtrl;
    Facebook.logout()
      .then(function(response) {
          NativeStorage.remove('user');
         // rootPage = LoginPage;
         // nav.push(LoginPage);
      }, function(error){
        console.log(error);
      });
  }



  /* 
   * Remover metodo apos teste
   */

  public testLocalNotification(){

        LocalNotifications.schedule({
           id: 1,
           text: 'Delayed ILocalNotification',
           at: new Date(new Date().getTime() + 3600),
           sound: null
        });
    }

}
