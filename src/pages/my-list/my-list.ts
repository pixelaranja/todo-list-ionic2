import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController} from 'ionic-angular';
import { NativeStorage } from 'ionic-native';
import { TaskService } from './../../providers/task-service';
import { TaskLoader } from './../../providers/task-loader';
import { Preferences } from './../../providers/preferences';
import { TaskModalPage } from './../task-modal/task-modal';


/*
  Generated class for the MyList page.
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
  selector: 'page-my-list',
  templateUrl: 'my-list.html',
  providers: [NativeStorage,Preferences]
})
export class MyListPage {

  todolist: Array<any> 

  constructor(public navCtrl: NavController, public navParams: NavParams, private taskService: TaskService, private alertCtrl: AlertController,
  private modalCtrl: ModalController, private taskLoader: TaskLoader, private preferences: Preferences) {
        this.findAll();
        
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyListPage');
  }


  /*
   *  LISTAS TODOS OS ITEMS DA LISTA DE TAREFAS 
   */
  findAll(){
    this.taskLoader.show();
    this.preferences.get().then((data: any) =>{
        this.taskService.taskAll(data.uuid).then((response: Array<any>) => {
            this.todolist = response;
            this.taskLoader.hide();
        }, (error) => {
            console.log('error:', error)
            this.taskLoader.hide();
        });
    }, (erro) => {
            console.log(erro);
    })
  }


  /* 
   * REMOVE O ITEM SELECIONADO 
   */
  removeTask(item){
   let alert = this.alertCtrl.create({
       title: 'Task',
       subTitle: 'Deseja deletar \'' + item.title + '\'?',
       buttons: [ 
                 {text: 'Cancel'},
                 {text: 'Delete', 
                   handler: (data) => {
                     this.taskLoader.show();
                      this.taskService.taskDelete(item.uuid)
                       .then((response) => {
                         this.findAll();
                         this.taskLoader.hide();
                       }, (error) => {
                         this.taskLoader.hide();
                          console.log("Error:", error);
                       });
                    }
                  }
                ]
    });
    alert.present();
  }




  /* 
   * ABRIR MODAL - ADICIONAR NOVO ITEM A LISTA
   */
  addTask(){
    let modal = this.modalCtrl.create(TaskModalPage,{
          create: true
       });
     modal.onDidDismiss(() => {
         this.findAll();
     });
     modal.present();
  }






  /* 
   *  ABRI O MODAL - ATUALIZAR O ITEM DA LISTA
   */
  updateTask(item){
    let modal = this.modalCtrl.create(TaskModalPage, {
        item: item
    });
    modal.onDidDismiss(() => {
        this.findAll();
    });
    modal.present();
  }

}
