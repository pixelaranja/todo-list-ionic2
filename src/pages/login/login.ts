import { NotificationEventResponse } from 'ionic-native/dist/esm';
import { Component, state, OnInit } from '@angular/core';
import { Facebook, NativeStorage } from 'ionic-native';
import { NavController, NavParams,ViewController, AlertController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { TaskService } from './../../providers/task-service';
import { UserService } from './../../providers/user-service';
import { TaskLoader } from './../../providers/task-loader';
import {WelcomePage} from '../../pages/welcome/welcome'


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[NativeStorage,TaskService,TaskLoader]
})


export class LoginPage {

  FB_APP_ID: number = 1382698145114827;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, private nativeStorage: NativeStorage, public userService: UserService,private taskLoader: TaskLoader,private alertCtrl: AlertController) {
    Facebook.browserInit(this.FB_APP_ID, "v2.8");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  /* 
   * LOGIN FACEBOOK
   */
  doFbLogin(){
    let permissions = new Array();
    permissions = ["public_profile","email"];
    let nav = this.navCtrl;
    let mUserService = this.userService;
    Facebook.login(permissions).then(function(response){
          let userId = response.authResponse.userID;
          let params = new Array();

          Facebook.api("/me?fields=id,name,first_name,email,gender", params)
              .then(function(user){
                user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";

                let obj = {"account":{ 
                    name: user.name, 
                    email: user.email, 
                    genre: user.gender,
                    password: user.id,
                    password_confirmation: user.id,
                    uid: user.id,
                    picture: user.picture
                  }};

                  console.log("******************",obj);

                  mUserService.create("/accounts", obj).then((response: any) => {
                    
                  

                          NativeStorage.setItem('user',
                                  {
                                    name: response.name,
                                    email: response.email,
                                    genre: response.genre,
                                    picture: response.picture,
                                    accesstoken: response.access_token, 
                                    uuid: response.uuid

                                  }).then(function(){
                                  }, function (error) {
                                    console.log(error);
                              })
                        }, (error) => {
                        console.log("Error:", error);
                      });

                      console.log("___________________,",response);
                      nav.push(HomePage);
              })}, function(error){
                console.log(error);
       });
  }


  /*
   * ALERTA
   */
    showAlert(msg){
      let alert = this.alertCtrl.create({
        title: '',
        subTitle: msg,
        buttons: [ "Ok" ]          
        });
        alert.present();
    }


}
