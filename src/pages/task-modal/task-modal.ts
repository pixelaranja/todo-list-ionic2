import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ViewContainer } from '@angular/compiler/src/private_import_core';
import { Component } from '@angular/core';
import { NativeStorage } from 'ionic-native';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { TaskService } from './../../providers/task-service';
import { TaskLoader } from './../../providers/task-loader';

/*
  Generated class for the TaskModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-task-modal',
  templateUrl: 'task-modal.html',
  providers: [TaskService, TaskLoader, NativeStorage]
})
export class TaskModalPage {

  private task: any;
  private todo: any;
  private obj: any;
  private labelButton: String;
  private isCreate: boolean;
  private static userUid: String;

  constructor(public navCtrl: NavController, public params: NavParams, private view: ViewController, private taskService: TaskService, private alertCtrl: AlertController,
   private taskLoader: TaskLoader) {
    this.task = {};
    this.todo = {};
    this.obj = {};
    this.isCreate = this.params.get("create");

    if(this.isCreate){
       this.labelButton = "Criar";
    }else{
      this.labelButton = "Atualizar";
    }
  }


  ionViewDidLoad() {
    if(!this.isCreate){
       let item = this.params.get("item");
       this.todo.title = item.title
       this.todo.description = item.description
       this.todo.date = item.date
       this.todo.time = item.date
       this.todo.notification = item.notification
     }else{
       this.getUUID();
     }
  }


  /* 
   * FECHA O MODAL
   */ 
  closeModal(){
    this.view.dismiss();
  }


  /* 
   * GET UUID 
   */
  getUUID(){
     NativeStorage.getItem('user')
          .then( function (data) {
              TaskModalPage.userUid = data.uuid;
              }, function (error) {
          });
  }


  /*
   * 
   */
  clickButton(){   
    this.obj = JSON.stringify({"task":{ 
      title: this.todo.title, 
      description: this.todo.description, 
      date: this.todo.date + " " + this.todo.time,
      notification:this.todo.notification  }});

    if(this.isCreate){
       this.save();
    }else{
      this.update();
    }
  }


  /* 
   * 
   */ 
  save(){
  
      if(this.todo.title == undefined){
        this.showAlert("Título não pode ser vazio!");
        return
      }

      if(this.todo.description == undefined){
        this.showAlert("Descriã não pode ser vazio!");
        return
      }

      if(this.todo.date == undefined){
        this.showAlert("Data não pode ser vazio!");
        return
      }

      if(this.todo.time == undefined){
        this.showAlert("Hora não pode ser vazio!");
        return
      }

      this.taskLoader.show();

      this.taskService.taskPost("/users/"+TaskModalPage.userUid+"/tasks",this.obj).then((response) => {
                    console.log(response);
                    this.taskLoader.hide();
                    this.view.dismiss();
                    }, (error) => {
                    console.log("Error:", error);
                    this.taskLoader.hide();
                    this.showAlert(error);
                  });
   }


  
   /* 
    * ATUALIZAR ITEM DA LISTA
    */
   update(){
      this.taskLoader.show();
      this.taskService.taskUpdate("/tasks/", this.params.get("item").uuid, this.obj)
         .then( (response) => { 
            this.taskLoader.hide();
            console.log(response);
            this.view.dismiss();
         },(erro) => {
            this.taskLoader.hide();
            console.log(erro);
      });
   }


   /*
    * ALERTA
   */
    showAlert(msg){
      let alert = this.alertCtrl.create({
        title: '',
        subTitle: msg,
        buttons: [ "Ok" ]          
        });
        alert.present();
    }

    

}
