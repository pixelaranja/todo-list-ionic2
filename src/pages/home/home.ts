import { AngularProfiler } from '@angular/platform-browser/src/browser/tools/common_tools';
import { MyApp } from '../../app/app.component';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {MyListPage} from './../my-list/my-list'
import {ProfilePage} from './../profile/profile'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tabRoot1: any; 
  tabRoot2: any;

  constructor(public navCtrl: NavController) {
    this.tabRoot1 = MyListPage;
    this.tabRoot2 = ProfilePage
  }
}
